# Apply

This project has been developed as part of my candidature in Gladys.
The test consisted in develop, without using any framework, an application with a data model composed by two linked entities.

For that purpose, I implemented MVC architecture in PHP, and coded routing system based on regex.